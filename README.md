#Feira API

> **Info** para criação dessa API foi usado banco de dados POSTGRES, então é necessario criar um B.D. com nome de "feiralivre" o codigo abaixo faz essa criação


    CREATE DATABASE feiralivre
      WITH OWNER = postgres
           ENCODING = 'UTF8'
           TABLESPACE = pg_default
           LC_COLLATE = 'Portuguese, Brazil'
           LC_CTYPE = 'Portuguese, Brazil'
           CONNECTION LIMIT = -1;

#Configuração
Utilize o comando abaixo para instalar as dependencias

    pip install -r requirements.txt

Dentro do arquivo settings.py na linha 78 altere para sua base local


    DATABASES = {
        'default':{
            'ENGINE':'django.db.backends.postgresql_psycopg2',
            'NAME':'feiralivre',
            'USER':'postgres',
            'PASSWORD':'q1w2e3r4',
            'HOST':'localhost',
            'PORT':'5432'
            }
    }


#Instalação
Baixe o diretorio feiraAPI e dentre dele execute

    python manage.py makemigrations feiraAPP

    python manage.py migrate

#Estrutura da API

    feiraAPI
    |
    +--feiraAPI ** arquivos configuração django
    +--feiraAPP ** arquivos do APP
        |--migrations ** arquivos gerados no manager migration
        |--testes ** arquivos de execucao de teste funcionalidades
    +--logs ** arquivo logs.txt grava dados eventos dos requests
    +--scriptbd ** migracao de dados
    +--template ** não usado neste caso


#Importação dos dados
Dentro da pasta scriptbd modifique as configurações da linha 12 para seu BD

    con = psycopg2.connect(database="feiralivre", user="postgres", password="q1w2e3r4", host="localhost", port="5432")

Dentro da pasta scriptbd executar, ele usar o arquivo DEINFO_AB_FEIRASLIVRES_2014.csv para fazer a importação dentro do banco

    py importacao.py

#Testes
Cadastro de uma nova feira executar

    py feiraAPP/teste1.py

Exclusão de uma feira através de seu código de registro

    py feiraAPP/teste2.py

Alteração dos campos cadastrados de uma feira, exceto seu código de

    py feiraAPP/teste3.py

Busca de feiras utilizando ao menos um dos parâmetros

    py feiraAPP/teste4.py