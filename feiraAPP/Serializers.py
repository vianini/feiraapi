from rest_framework import serializers
from .models import FeiraLivre


class FeiraLivreSerializer(serializers.ModelSerializer):
    class Meta:
        model = FeiraLivre
        fields = (
            'id',
            'longitude',
            'latitude',
            'setcens',
            'areap',
            'coddist',
            'distrito',
            'codsubpref',
            'subprefe',
            'regiao5',
            'regiao8',
            'nome_feira',
            'registro',
            'logradouro',
            'numero',
            'bairro',
            'referencia'
        )
