from django.db import models

class FeiraLivre(models.Model):

    longitude = models.BigIntegerField()
    latitude =models.BigIntegerField()
    setcens = models.BigIntegerField()
    areap = models.BigIntegerField()
    coddist = models.IntegerField()
    distrito = models.CharField(max_length=100)
    codsubpref = models.IntegerField()
    subprefe = models.CharField(max_length=100)
    regiao5 = models.CharField(max_length=100)
    regiao8 = models.CharField(max_length=100)
    nome_feira = models.CharField(max_length=100)
    registro = models.CharField(max_length=100)
    logradouro = models.CharField(max_length=100)
    numero = models.CharField(max_length=100,null=True, blank=True)
    bairro = models.CharField(max_length=100,null=True, blank=True)
    referencia = models.CharField(max_length=100,null=True, blank=True)

    def __str__(self):
        return '%s' % self

    class Meta:
        db_table = 'tbl_feiralivre'

