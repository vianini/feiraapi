from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import FeiraLivre
from .Serializers import FeiraLivreSerializer


# import the logging library
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)

@api_view(['GET', 'POST', 'PUT', 'DELETE'])
def feiralivre(request):

    if request.method == 'POST':

        feira = FeiraLivre(**request.data)

        if verifica_integridade(feira):
            feira.save()
            logger.info("METODO:POST,FEIRA:,registro criado")
            return Response("registro criado")
        else:
            return Response("registro existente.")

    if request.method == 'PUT':
        feira = FeiraLivreSerializer(data=request.data)
        feiranova = FeiraLivre(**request.data)

        if feira.is_valid() and verifica_integridade(feiranova):
            nova = FeiraLivre.objects.get(id=feiranova.id)

            nova.longitude = feiranova.longitude
            nova.latitude = feiranova.latitude
            nova.setcens = feiranova.setcens
            nova.areap = feiranova.areap
            nova.coddist = feiranova.coddist
            nova.distrito = feiranova.distrito
            nova.codsubpref = feiranova.codsubpref
            nova.subprefe = feiranova.subprefe
            nova.regiao5 = feiranova.regiao5
            nova.regiao8 = feiranova.regiao8
            nova.nome_feira = feiranova.nome_feira
            nova.registro = feiranova.registro
            nova.logradouro = feiranova.logradouro
            nova.numero = feiranova.numero
            nova.bairro = feiranova.bairro
            nova.referencia = feiranova.referencia

            nova.save()
            logger.info("METODO:PUT,ID:"+request.GET.get('id')+",registro salvo")
            return Response("registro salvo")
        else:
            return Response("registro não salvo")

    if request.method == 'GET':
        if request.GET.get('id') != None:
               delfeira = FeiraLivre.objects.get(id=request.GET.get('id'))
               delfeira.delete()
               logger.info("METODO:GET_DELETE,ID:"+request.GET.get('id'))
               return Response("deletado")



    if request.method == 'GET':
        distrito = (request.GET.get('distrito') if request.GET.get('distrito') != None else "%%")
        regiao5 = (request.GET.get('regiao5') if request.GET.get('regiao5') != None else "%%")
        nome_feira = (request.GET.get('nome_feira') if request.GET.get('nome_feira') != None else "%%")
        bairro = (request.GET.get('bairro') if request.GET.get('bairro') != None else "%%")

        sql = "select * from tbl_feiralivre " \
              "where " \
              "distrito like %s and " \
              "regiao5 like  %s and " \
              "nome_feira like %s and " \
              "bairro like %s "
        queryset = FeiraLivre.objects.raw(sql, (distrito, regiao5, nome_feira, bairro))
        feiras = FeiraLivreSerializer(queryset, many=True)
        logger.info("METODO:GET,PARAMETROS:{distrito:"+distrito+",distrito:"+distrito+",regiao5:"+regiao5+",nome_feira:"+nome_feira+",bairro:"+bairro+"} ,QTD_FEIRAS:" +str(len(feiras.data)))
        return Response(feiras.data)


def verifica_integridade(feira):
    sql = "select * from tbl_feiralivre where " \
          "longitude = %s and " \
          "latitude = %s and " \
          "setcens = %s and " \
          "areap = %s and " \
          "coddist = %s and " \
          "distrito = %s and " \
          "codsubpref = %s and " \
          "subprefe = %s and " \
          "regiao5 = %s and " \
          "regiao8 = %s and " \
          "nome_feira = %s and " \
          "registro = %s and " \
          "logradouro = %s and " \
          "numero = %s and " \
          "bairro = %s and " \
          "referencia = %s "
    queryset = FeiraLivre.objects.raw(
        sql,
        (
            feira.longitude,
            feira.latitude,
            feira.setcens,
            feira.areap,
            feira.coddist,
            feira.distrito,
            feira.codsubpref,
            feira.subprefe,
            feira.regiao5,
            feira.regiao8,
            feira.nome_feira,
            feira.registro,
            feira.logradouro,
            feira.numero,
            feira.bairro,
            feira.referencia
        )
    )
    feirateste = FeiraLivreSerializer(queryset, many=True)
    if len(feirateste.data) == 0:
        return True
    else:
        return False
