
import requests
import json

vjson = {
        "id":56,
        "longitude": -46485001,
        "latitude": -23568662,
        "setcens": 355030824000042,
        "areap": 3550308005153,
        "coddist": 24,
        "distrito": "CIDADE teste",
        "codsubpref": 22,
        "subprefe": "ITAQUERA",
        "regiao5": "Leste",
        "regiao8": "Leste 2",
        "nome_feira": "PARQUE SAVOY CITY",
        "registro": "7210-9",
        "logradouro": "RUA DR CELSO PACHECO BETIM",
        "numero": "S/N",
        "bairro": "PQ SAVOY CITY",
        "referencia": ""
    }


url = "http://127.0.0.1:8000/feiralivre/"

headers = {'content-type': 'application/json'}

response = requests.put(url, data=json.dumps(vjson), headers=headers)

print(response)