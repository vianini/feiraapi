import csv
import psycopg2
from psycopg2._psycopg import IntegrityError


def processa():
    filename = r"DEINFO_AB_FEIRASLIVRES_2014.csv"

    f = open(filename, 'rtU')
    csv_f = csv.DictReader(f, quotechar="\"", delimiter=',')

    con = psycopg2.connect(database="feiralivre", user="postgres", password="q1w2e3r4", host="localhost", port="5432")
    con.autocommit = True
    cur = con.cursor()


    inserts = 0
    updates = 0
    erros = 0
    for line in csv_f:
        try:

            sql =r"INSERT INTO tbl_feiralivre(" \
                 "id, " \
                 "longitude, " \
                 "latitude, " \
                 "setcens, " \
                 "areap, " \
                 "coddist, " \
                 "distrito, " \
                 "codsubpref," \
                 "subprefe, " \
                 "regiao5, " \
                 "regiao8, " \
                 "nome_feira, " \
                 "registro, " \
                 "logradouro," \
                 "numero, " \
                 "bairro, " \
                 "referencia) " \
                 "VALUES ( " \
                  +line['ID'] +"," \
                  +line['LONG']+"," \
                  +line['LAT']+"," \
                  +line['SETCENS']+"," \
                  +line['AREAP'] +"," \
                  +line['CODDIST'] +"," \
                  +"'"+str(line['DISTRITO']).replace("'","''")+"', " \
                  +line['CODSUBPREF']+"," \
                  +"'"+str(line['SUBPREFE']).replace("'","''")+"', " \
                  "'"+str(line['REGIAO5']).replace("'","''")+"', " \
                  "'"+str(line['REGIAO8']).replace("'","''")+"', " \
                  "'"+str(line['NOME_FEIRA']).replace("'","''")+"', " \
                  "'"+str(line["REGISTRO"]).replace("'","''")+"', " \
                  "'"+ str(line["LOGRADOURO"]).replace("'","''")+"', " \
                  + ("'" + line['NUMERO'] + "'" if line['NUMERO'] != None else 'null') + " , " \
                  + ("'" + str(line['BAIRRO']).replace("'","''") + "'" if line['BAIRRO'] != None else 'null') + " , " \
                  + ("'" + str(line['REFERENCIA']).replace("'","''") + "'" if line['REFERENCIA'] != None else 'null') +"); " \


            cur.execute(sql)
            inserts = 1 + inserts
        except IntegrityError as e:

            sql = r"UPDATE tbl_feiralivre SET " \
                  "longitude="+line['LONG']+", " \
                  "latitude="+line['LAT']+", " \
                  "setcens="+line['SETCENS']+", " \
                  "areap="+line['AREAP']+", " \
                  "coddist="+line['CODDIST']+", " \
                  "distrito='"+str(line['DISTRITO']).replace("'","''")+"', " \
                  "codsubpref="+line['CODSUBPREF']+", " \
                  "subprefe='"+str(line['SUBPREFE']).replace("'","''")+"', " \
                  "regiao5='"+str(line['REGIAO5']).replace("'","''")+"', " \
                  "regiao8='"+str(line['REGIAO8']).replace("'","''")+"', " \
                  "nome_feira='"+str(line['NOME_FEIRA']).replace("'","''")+"', " \
                  "registro='"+str(line['REGISTRO']).replace("'","''")+"', " \
                  "logradouro='" + str(line['LOGRADOURO']).replace("'","''") +"', " \
                  "numero=" + ("'" + line['NUMERO'] + "'" if line['NUMERO'] != None else 'null') + " , " \
                  "bairro=" + ("'" + str(line['BAIRRO']).replace("'","''") + "'" if line['BAIRRO'] != None else 'null') + ", " \
                  "referencia=" + ("'" + str(line['REFERENCIA']).replace("'","''") + "'" if line['REFERENCIA'] != None else 'null') + " " \
                  "WHERE id="+line['ID']+" ;"

            cur.execute(sql)
            updates = 1 + updates
            con.commit()
        except psycopg2.ProgrammingError as e:
            print(e)
            erros = 1 + erros

    con.close()
    print("RESUMO:{INSERT:"+str(inserts)+",UPDATE:"+str(updates)+",ERRO:"+str(erros)+"}")

if __name__ == '__main__':
    processa()